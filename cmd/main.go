package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"cloud.google.com/go/firestore"

	httptransport "api/internal/transport/http"
)

func main() {
	projectID := "hade-proj"

	firestoreClient, err := firestore.NewClient(context.Background(), projectID)

	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Close client when done.
	defer firestoreClient.Close()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}

	server := httptransport.CreateServer(firestoreClient)

	srv := &http.Server{
		Handler:      server.Router,
		Addr:         fmt.Sprintf(":%s", port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
