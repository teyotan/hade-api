## Important Todo
1. Sanitize input to only allow positive int at creating product stock event
2. Edit product stock event API

## Can wait Todo
1. Mock database client
2. Unit test