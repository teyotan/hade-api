package service

import (
	"errors"
	"log"
	"time"

	"cloud.google.com/go/firestore"
	"github.com/gin-gonic/gin"
	"github.com/mitchellh/mapstructure"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ErrIDNotFound ...
var ErrIDNotFound = errors.New("ID not found")

// Product ...
type Product struct {
	ID     string `json:"id,omitempty" firestore:"-"`
	Name   string `json:"name" firestore:"name"`
	Amount int    `json:"amount" firestore:"amount"`
}

// ProductEvent ...
type ProductEvent struct {
	ID          string    `json:"id" firestore:"-"`
	ProductID   string    `json:"-" firestore:"-"`
	Timestamp   time.Time `json:"timestamp" firestore:"timestamp"`
	Price       int       `json:"price" firestore:"price"`
	Amount      int       `json:"amount" firestore:"amount"`
	EventType   string    `json:"type" firestore:"type"`
	Description *string   `json:"description" firestore:"description"`
}

// CreateProduct ...
func (app *App) CreateProduct(c *gin.Context, name string) (Product, error) {
	product := Product{
		Name: name,
	}

	doc, _, err := app.db.Collection("products").Add(c, product)

	if err != nil {
		log.Printf("An error has occurred: %s", err)
	}

	product.ID = doc.ID

	return product, nil
}

// GetProduct ...
func (app *App) GetProduct(c *gin.Context, id string) (*Product, error) {
	dsnap, err := app.db.Collection("products").Doc(id).Get(c)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return nil, ErrIDNotFound
		}
		return nil, err
	}
	var product Product
	dsnap.DataTo(&product)

	return &product, nil
}

// ListProducts ...
func (app *App) ListProducts(c *gin.Context) ([]Product, error) {
	iter := app.db.Collection("products").Documents(c)
	result := []Product{}
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Printf("%s", err)
		}

		var product Product
		doc.DataTo(&product)
		product.ID = doc.Ref.ID

		result = append(result, product)
	}

	return result, nil
}

// DeleteProduct ...
func (app *App) DeleteProduct(c *gin.Context, id string) error {
	_, err := app.db.Collection("products").Doc(id).Delete(c)
	if err != nil {
		log.Printf("An error has occurred: %s", err)
	}

	return nil
}

// CreateProductEvent ...
func (app *App) CreateProductEvent(c *gin.Context, request map[string]interface{}) (ProductEvent, error) {
	var productEvent ProductEvent

	mapstructure.Decode(request, &productEvent)

	if productEvent.Timestamp.IsZero() {
		productEvent.Timestamp = time.Now().UTC()
	}

	doc, _, err := app.db.Collection("products").Doc(productEvent.ProductID).Collection("stockEvents").Add(c, productEvent)

	if err != nil {
		log.Printf("An error has occurred: %s", err)
	}

	productEvent.ID = doc.ID

	err = app.RehydrateProductAmount(c, productEvent.ProductID)

	if err != nil {
		log.Printf("An error has occurred: %s", err)
	}

	return productEvent, nil
}

// ListProductEvent ...
func (app *App) ListProductEvent(c *gin.Context, productID string) ([]ProductEvent, error) {
	iter := app.db.Collection("products").Doc(productID).Collection("stockEvents").Documents(c)
	result := []ProductEvent{}
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Printf("%s", err)
		}

		var productEvent ProductEvent
		doc.DataTo(&productEvent)
		productEvent.ID = doc.Ref.ID

		result = append(result, productEvent)
	}

	return result, nil
}

// DeleteProductEvent ...
func (app *App) DeleteProductEvent(c *gin.Context, productID, productEventID string) error {

	_, err := app.db.Collection("products").Doc(productID).Collection("stockEvents").Doc(productEventID).Delete(c)
	if err != nil {
		log.Printf("An error has occurred: %s", err)
	}

	err = app.RehydrateProductAmount(c, productID)

	if err != nil {
		log.Printf("An error has occurred: %s", err)
	}

	return nil
}

// RehydrateProductAmount ...
func (app *App) RehydrateProductAmount(c *gin.Context, productID string) error {
	productEvents, _ := app.ListProductEvent(c, productID)

	amount := 0
	for _, productEvent := range productEvents {
		if productEvent.EventType == "inc" {
			amount += productEvent.Amount
		} else if productEvent.EventType == "dec" {
			amount -= productEvent.Amount
		}
	}

	_, err := app.db.Collection("products").Doc(productID).Set(c, map[string]interface{}{
		"amount": amount,
	}, firestore.MergeAll)

	if err != nil {
		log.Printf("An error has occurred: %s", err)
	}

	return nil
}
