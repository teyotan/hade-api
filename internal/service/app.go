package service

import "cloud.google.com/go/firestore"

// App type
type App struct {
	db *firestore.Client
}

// CreateApp ...
func CreateApp(db *firestore.Client) *App {
	return &App{
		db,
	}
}
