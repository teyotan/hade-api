package http

import (
	"net/http"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"

	"api/internal/service"

	"github.com/fatih/structs"
	"github.com/gin-gonic/gin"
)

// CreateProductEndpoint ...
func (app *Endpoint) CreateProductEndpoint() gin.HandlerFunc {
	type CreateProductRequest struct {
		Name string `json:"name" binding:"required"`
	}

	return func(c *gin.Context) {
		var request CreateProductRequest
		if err := c.ShouldBindJSON(&request); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		result, err := app.CreateProduct(c, request.Name)
		if err != nil {
			return
		}

		c.JSON(201, gin.H{
			"id":   result.ID,
			"name": result.Name,
		})
	}
}

// GetProductEndpoint ...
func (app *Endpoint) GetProductEndpoint() gin.HandlerFunc {
	type response struct {
		Name     string      `json:"name"`
		Amount   int         `json:"amount"`
		Includes interface{} `json:"includes,omitempty"`
	}

	return func(c *gin.Context) {
		productID := c.Param("productID")

		product, err := app.GetProduct(c, productID)
		if err != nil {
			if err == service.ErrIDNotFound {
				c.JSON(404, gin.H{
					"errors": err.Error(),
				})
			}
			return
		}

		var result response
		mapstructure.Decode(product, &result)

		include := c.Query("include")
		if len(include) > 0 {
			var includes = make(map[string]interface{})
			for _, val := range strings.Split(include, ",") {
				switch val {
				case "stockEvent":
					stockEvents, err := app.ListProductEvent(c, productID)
					if err != nil {
						return
					}
					includes["stockEvent"] = stockEvents
				}
			}
			result.Includes = includes
		}

		c.JSON(200, result)

	}
}

// ListProductsEndpoint ...
func (app *Endpoint) ListProductsEndpoint() gin.HandlerFunc {
	return func(c *gin.Context) {
		result, err := app.ListProducts(c)
		if err != nil {
			return
		}

		c.JSON(200, gin.H{
			"data": result,
		})
	}
}

// DeleteProductEndpoint ...
func (app *Endpoint) DeleteProductEndpoint() gin.HandlerFunc {
	return func(c *gin.Context) {
		productID := c.Param("productID")

		err := app.DeleteProduct(c, productID)
		if err != nil {
			return
		}

		c.JSON(204, gin.H{})
	}
}

// CreateProductEventEndpoint ...
func (app *Endpoint) CreateProductEventEndpoint() gin.HandlerFunc {
	type request struct {
		ProductID   string     `json:"-" binding:"-"`
		Amount      *int       `json:"amount" binding:"required"`
		Price       *int       `json:"price" binding:"required"`
		EventType   string     `json:"type" binding:"required"`
		Timestamp   *time.Time `json:"timestamp" binding:"-"`
		Description *string    `json:"description" binding:"-"`
	}

	var allowedType = map[string]bool{
		"inc": true,
		"dec": true,
	}

	return func(c *gin.Context) {
		var request request
		if err := c.ShouldBindJSON(&request); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		if val := allowedType[request.EventType]; !val {
			c.JSON(http.StatusBadRequest, gin.H{"error": "unknown type"})
			return
		}

		request.ProductID = c.Param("productID")

		productEvent, err := app.CreateProductEvent(c, structs.Map(request))

		if err != nil {
			return
		}

		c.JSON(201, productEvent)

	}
}

// ListProductEventsEndpoint ...
func (app *Endpoint) ListProductEventsEndpoint() gin.HandlerFunc {
	return func(c *gin.Context) {
		result, err := app.ListProductEvent(c, c.Param("productID"))
		if err != nil {
			return
		}

		c.JSON(200, gin.H{
			"data": result,
		})
	}
}

// DeleteProductEventEndpoint ...
func (app *Endpoint) DeleteProductEventEndpoint() gin.HandlerFunc {
	return func(c *gin.Context) {
		productID := c.Param("productID")
		productEventID := c.Param("productEventID")

		if err := app.DeleteProductEvent(c, productID, productEventID); err != nil {
			return
		}

		c.JSON(204, gin.H{})
	}
}
