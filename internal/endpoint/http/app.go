package http

import (
	"api/internal/service"
)

// Endpoint type extending service App
type Endpoint struct{ *service.App }

// CreateEndpoint ...
func CreateEndpoint(app *service.App) *Endpoint {
	return &Endpoint{
		app,
	}
}
