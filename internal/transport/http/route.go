package http

func setRoute(s *Server) {
	s.Router.GET("/product", s.Endpoint.ListProductsEndpoint())
	s.Router.GET("/product/:productID", s.Endpoint.GetProductEndpoint())
	s.Router.POST("/product", s.Endpoint.CreateProductEndpoint())
	s.Router.DELETE("/product/:productID", s.Endpoint.DeleteProductEndpoint())

	s.Router.GET("/product/:productID/stock", s.Endpoint.ListProductEventsEndpoint())
	s.Router.POST("/product/:productID/stock", s.Endpoint.CreateProductEventEndpoint())
	s.Router.DELETE("/product/:productID/stock/:productEventID", s.Endpoint.DeleteProductEventEndpoint())
}
