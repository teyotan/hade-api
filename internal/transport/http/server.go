package http

import (
	httpendpoint "api/internal/endpoint/http"
	"api/internal/service"

	"cloud.google.com/go/firestore"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// Server ...
type Server struct {
	Endpoint *httpendpoint.Endpoint
	Router   *gin.Engine
}

// CreateServer return a Server instance
func CreateServer(firestoreClient *firestore.Client) *Server {
	app := service.CreateApp(firestoreClient)

	server := Server{
		Endpoint: httpendpoint.CreateEndpoint(app),
		Router:   gin.New(),
	}

	server.Router.Use(cors.Default())

	setRoute(&server)
	return &server
}
